# Create your views here.
# from django.template.loader import get_template
# from django.views.generic.base import TemplateView

from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect 
# ^ last for forms
from django.core.context_processors import csrf
from django.template import RequestContext

from django.shortcuts import get_object_or_404 
from django.shortcuts import render
from django.shortcuts import redirect

from forms import clientnewForm
from forms import productnewForm
from forms import dealnewForm
from forms import catnewForm

from plug.models import clients
from plug.models import products
from plug.models import deals
from plug.models import productsCategory
from django.core.exceptions import ObjectDoesNotExist



# define
def home(request):
	entries = clients.objects.all()[:10]
	return render_to_response('index.html', {'clients' : entries})


def client(request):
	return render_to_response('client.html', {'clients' : clients.objects.all() })
	# name = "Vladislav"
	# return render_to_response('client.html', {'name' : name})
	# html = "<html><body>Hi %s, this is working!</body></html>" % name
	# return HttpResponse(html)
	
	

def product(request):
	return render_to_response('present.html', {'products' : products.objects.all() })
	#products.objects.values('category2', 'charact') })


def deal(request):
	return render_to_response('deals.html', {'deals' : deals.objects.all() })

	
def product_needed(request, products_id=1, category=None):
#def product_needed(request, products_category2):
	#p=products(category2='Pens')
	return render_to_response('presentget.html', {'products' : #products.objects.all() })
	#p.objects })
# https://docs.djangoproject.com/en/1.7/topics/db/managers/#modifying-initial-manager-querysets

#	products.needed_objects.filter(title='Pens')
#	products.objects.get(category2=products_category2) })

#	products.objects.filter(products__category2=products_category2) })
	products.objects.filter(category2='Pens') }) # only 1 object
#	products.objects.filter(productscategory__category=category_id) })
#	products.objects.get(id=products_id) })
#	^

#	products.objects.filter(id='2') })


def list_products(request, category=None):
    template_name = 'presentlist.html'
    products_list = products.objects.all()
    clients_list = clients.objects.all()

	#.order_by('name') # this is for the list of category names to select from in the template

    if request.method == 'POST':
        category_id = request.POST['category']
        try:
            # here are the joins
            #clients_list = clients.objects.filter(productscategory__category=category_id)
            products_list = products.objects.filter(productscategory__category=category_id)

        except ObjectDoesNotExist:
            clients_list = None
    else:
        # if there's no category_id passed in, just return the whole list
        clients_list = clients.objects.all()
		#.order_by('-added')

    return render_to_response(template_name, { 'products_list': products_list, 'clients_list': clients_list, }, context_instance=RequestContext(request))



def list_products2(request, category=None):
    #template_name = 'presentlist.html'
    template_name = 'presentgett.html'
    products_list = products.objects.all()
    #clients_list = clients.objects.all()

	#.order_by('name') # this is for the list of category names to select from in the template

    if request.method == 'POST':
        category_id = request.POST['category']
        try:
            # here are the joins
            #clients_list = clients.objects.filter(productscategory__category=category_id)
            #products_list = products.objects.filter(productscategory__category=category_id)
            #products_list = products.objects.filter(productscategory__category__exact=category_id)
	        #category_list = productsCategory.objects.filter(productscategory__category__exact=category_id)
            products_list = products.objects.filter(category2=category_id)
            #products_list = products.objects.filter(productscategory__category=request.POST['category'])
			#products_list = products.objects.get(pk=request.POST['category'])	#not iterable	
            #products_list = products.objects.filter(productscategory__category=category_id)
			#products_list = products.objects.filter(category2=category_category2)

        except ObjectDoesNotExist:
            #clients_list = None
			products_list = None
    else:
        # if there's no category_id passed in, just return the whole list
       # clients_list = clients.objects.all()
		products_list = products.objects.all()
		#.order_by('-added')

    return render_to_response(template_name, { 'products_list': products_list,
	#'clients_list': clients_list, 
	#'category_list': category_list, 
	}, context_instance=RequestContext(request))


def search_titles(request):
    articles = SearchQuerySet().autocomplete(content_auto=request.POST.get('search_text', ''))            
    
    return render_to_response('ajax_search.html', {'articles' : articles})


	
def createclient(request):
	if request.POST:
		form = clientnewForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/client')
	else:
		form = clientnewForm()		
	args = {}
	args.update(csrf(request))
	args['form'] = form
	#return render_to_response('clientnew.html', {'clients' : clients.objects.all() })
	return render_to_response('clientnew.html', args)

	
def editclient(request, pk):
    post = get_object_or_404(clients, pk=pk)
    if request.method == "POST":
        form = clientnewForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return HttpResponseRedirect('/client')
			#return redirect('plug.views.client', pk=post.pk)
    else:
        form = clientnewForm(instance=post)
    return render(request, 'editclient.html', {'form': form})
	
	
def createproduct(request):
	if request.POST:
		form = productnewForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/present')
	else:
		form = productnewForm()		
	args = {}
	args.update(csrf(request))
	args['form'] = form
	return render_to_response('productnew.html', args)
	
	
def editproduct(request, pk):
    post = get_object_or_404(products, pk=pk)
    if request.method == "POST":
        form = productnewForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return HttpResponseRedirect('/present')
			#return redirect('plug.views.client', pk=post.pk)
    else:
        form = productnewForm(instance=post)
    return render(request, 'editproduct.html', {'form': form})

	
def createdeal(request):
	if request.POST:
		form = dealnewForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/deals')
	else:
		form = dealnewForm()		
	args = {}
	args.update(csrf(request))
	args['form'] = form
	return render_to_response('dealnew.html', args)
	
	
def createcat(request):
	if request.POST:
		form = catnewForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/present/createproduct')
	else:
		form = catnewForm()		
	args = {}
	args.update(csrf(request))
	args['form'] = form
	return render_to_response('catnew.html', args)