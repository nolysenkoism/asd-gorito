from django.db import models
from time import time
from datetime import datetime

# Create your models here.

#class CategoryMain(models.Model):
 #   #name = models.CharField(max_length=40, unique=True)
  #  name = models.TextField(unique=True)
   # def __unicode__(self):
	#	return self.name

def get_upload_file_name(instance, filename):
	return "uploaded_files/%s_%s" % (str(time()).replace('.','_'), filename)
	
class productsCategory(models.Model):
    #article = models.ForeignKey('clients', null=True, blank=True)
    category = models.CharField(max_length=20, unique=True) # check primary_key=True
    def __unicode__(self):
		return unicode(self.category)
		#self.recorded_on.strftime('%Y-%m-%d')
		#unicode(self.some_field) or u''
		
	
class clients(models.Model):
	# description
	name1 = models.CharField(max_length = 30, unique=True)
	addre1 = models.CharField(max_length = 50)
	company1 = models.CharField(max_length = 50)
	category1 = models.ForeignKey(productsCategory)
	finances1 = models.IntegerField()

	def __unicode__(self):
		return self.name1

class products(models.Model):
	category2 = models.ForeignKey(productsCategory)
	name2 = models.CharField(max_length = 30, unique=True)
	descr = models.TextField()
	charact = models.TextField()
	price = models.IntegerField()
	#picture = models.FileField(upload_to=get_upload_file_name)
	
	def __unicode__(self):
		return self.name2
		
class deals(models.Model):
	name3 = models.ForeignKey(clients)
	#date = models.DateTimeField(auto_now_add=True, blank=False)
	date = models.DateTimeField(default=datetime.now, blank=True)
	prod = models.ForeignKey(products)
	amount = models.PositiveSmallIntegerField(default=1)
	price = models.IntegerField()

	def __unicode__(self):
		return unicode(self.name3)
		
