from django import forms
from models import clients
from models import products
from models import deals
from models import productsCategory


class clientnewForm(forms.ModelForm):

	class Meta:
		model = clients
		# form = autocomplete_light.clientnewForm(fields='__all__')
		fields = "__all__"
		

class productnewForm(forms.ModelForm):

	class Meta:
		model = products
		fields = "__all__"
		
		
class dealnewForm(forms.ModelForm):

	class Meta:
		model = deals
		fields = "__all__"
		

class catnewForm(forms.ModelForm):

	class Meta:
		model = productsCategory
		fields = "__all__"