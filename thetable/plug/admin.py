from django.contrib import admin

# Register your models here.

from plug.models import clients
from plug.models import products

admin.site.register(clients)
admin.site.register(products)

from plug.models import productsCategory

admin.site.register(productsCategory)

from plug.models import deals

admin.site.register(deals)