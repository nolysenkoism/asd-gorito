from django.db import models
from time import time
from datetime import datetime

# Create your models here.

#class CategoryMain(models.Model):
 #   #name = models.CharField(max_length=40, unique=True)
  #  name = models.TextField(unique=True)
   # def __unicode__(self):
	#	return self.name

def get_upload_file_name(instance, filename):
	return "uploaded_files/%s_%s" % (str(time()).replace('.','_'), filename)
	
class productsCategory(models.Model):
    #article = models.ForeignKey('clients', null=True, blank=True)
    category = models.CharField(max_length=20, unique=True) # check primary_key=True
    def __unicode__(self):
		return unicode(self.category)
		#self.recorded_on.strftime('%Y-%m-%d')
		#unicode(self.some_field) or u''
		
	
class clients(models.Model):
	# description
	name1 = models.CharField(max_length = 30, unique=True, verbose_name="Name of client")
	addre1 = models.CharField(max_length = 50, verbose_name="Address")
	company1 = models.CharField(max_length = 50, verbose_name="Name of company")
	category1 = models.ForeignKey(productsCategory, verbose_name="Preferred products")
	finances1 = models.IntegerField(verbose_name="Finances")

	def __unicode__(self):
		return self.name1

class products(models.Model):
	category2 = models.ForeignKey(productsCategory, verbose_name="Product category")
	name2 = models.CharField(max_length = 30, unique=True, verbose_name="Product name")
	descr = models.TextField(verbose_name="Description")
	charact = models.TextField(verbose_name="Main characteristics")
	price = models.IntegerField()
	#picture = models.FileField(upload_to=get_upload_file_name)
	
	def __unicode__(self):
		return self.name2
		
class deals(models.Model):
	name3 = models.ForeignKey(clients, verbose_name="Name of client")
	#date = models.DateTimeField(auto_now_add=True, blank=False)
	date = models.DateTimeField(default=datetime.now, blank=True)
	prod = models.ForeignKey(products, verbose_name="Chosen product")
	amount = models.PositiveSmallIntegerField(default=1)
	price = models.IntegerField()

	def __unicode__(self):
		return unicode(self.name3)
		
