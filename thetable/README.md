ASD
===

Gorito codobe xydo sellhub
===

/// Добавление нового клиента
===

![screen createclient](https://cloud.githubusercontent.com/assets/9483826/5159528/4f5572e6-7371-11e4-963d-a35569d0a136.png)

Косо выглядит < работает.

Стилизация возможна, но несколько нетривиальна - 

http://stackoverflow.com/questions/5827590/css-styling-in-django-forms

https://docs.djangoproject.com/en/1.7/topics/forms/#working-with-form-templates

https://www.chicagodjango.com/blog/django-form-snippets/


/// Немного свежего фронтенда
===

![screenshot from 2014-11-19 19-11-06](https://cloud.githubusercontent.com/assets/9483826/5110305/41522b0c-7020-11e4-9415-e9192beac850.png)
БД - sqlite3.


/// Печать первой таблицы
===

![screenshot from 2014-11-18 17-42-47](https://cloud.githubusercontent.com/assets/9483826/5090864/c144e210-6f4d-11e4-9b95-6af8b2ee8ad8.png)

По ней можно сделать сортировку, я тоже займусь.

Таблицы стилей и скрипты в static/, при работе питон к ним обращается через assets/ - туда нет смысла что-то заливать, оно копируется для безопасности и чистоты работы (с)

thetable/templates/ и plug/templates/ - собственно html файлы, к которым они применяются. Пока активны index.html и client.html.

style1.css - мой для удобства, просто обводка и расположение.
Колонка перемещена в таблице, не в бд. Самой бд тут нет - если использовать mysql, а не sqlite3, дефолтный файл которой в корне папки.
