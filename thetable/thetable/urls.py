from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
	#(r'^client/', include('plug.urls')),
    url(r'^client/$', 'plug.views.client'),
    url(r'^present/search', 'plug.views.product'),
	url(r'^present/$', 'plug.views.list_products2'),

	url(r'^client/createclient/', 'plug.views.createclient'),
	url(r'^present/createproduct/', 'plug.views.createproduct'),
	url(r'^deals/newdeal/', 'plug.views.createdeal'),
	url(r'^present/createcat', 'plug.views.createcat'),

	url(r'^client/(?P<pk>[0-9]+)/edit/$', 'plug.views.editclient', name='editclient'),
	url(r'^present/(?P<pk>[0-9]+)/edit/$', 'plug.views.editproduct', name='editproduct'),

	#url(r'^present/get/(?P<products_category2>\d+)/$', 'plug.views.product_needed'),

	url(r'^present/get/(?P<products_id>\d+)/$', 'plug.views.product_needed'),
	#url(r'^client/$', 'plug.views.list_products'),
	url(r'^present/getr/', 'plug.views.list_products'),
	#url(r'^present/gett/', 'plug.views.list_products2'),

	###
	
    # Examples:
    url(r'^$', 'plug.views.home', name='home'),
    # url(r'^$', 'plug.views.product', name='product'),
    url(r'^deals/$', 'plug.views.deal'),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
